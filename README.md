# YuMall Notice 

> 这个通知服务的gRPC协议包

编译go
```
protoc ./*.proto --go_out=plugins=grpc:go-src
```

编译PHP
```
protoc --php_out=php-src/ ./*.proto
```