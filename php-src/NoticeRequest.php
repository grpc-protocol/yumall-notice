<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: message.proto

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>NoticeRequest</code>
 */
class NoticeRequest extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>.MessageType type = 1;</code>
     */
    private $type = 0;
    /**
     * Generated from protobuf field <code>string from = 2;</code>
     */
    private $from = '';
    /**
     * Generated from protobuf field <code>map<string, string> to = 3;</code>
     */
    private $to;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type int $type
     *     @type string $from
     *     @type array|\Google\Protobuf\Internal\MapField $to
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Message::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>.MessageType type = 1;</code>
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Generated from protobuf field <code>.MessageType type = 1;</code>
     * @param int $var
     * @return $this
     */
    public function setType($var)
    {
        GPBUtil::checkEnum($var, \MessageType::class);
        $this->type = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string from = 2;</code>
     * @return string
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * Generated from protobuf field <code>string from = 2;</code>
     * @param string $var
     * @return $this
     */
    public function setFrom($var)
    {
        GPBUtil::checkString($var, True);
        $this->from = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>map<string, string> to = 3;</code>
     * @return \Google\Protobuf\Internal\MapField
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * Generated from protobuf field <code>map<string, string> to = 3;</code>
     * @param array|\Google\Protobuf\Internal\MapField $var
     * @return $this
     */
    public function setTo($var)
    {
        $arr = GPBUtil::checkMapField($var, \Google\Protobuf\Internal\GPBType::STRING, \Google\Protobuf\Internal\GPBType::STRING);
        $this->to = $arr;

        return $this;
    }

}

